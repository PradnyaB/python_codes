class Parent:
    def __init__(self):
        print("In Parent constructor")

    def DispData(self):
        print("In Parent instance method")
    @classmethod
    def printData(cls):
        print("In classMethod : Parent")

    @staticmethod
    def getData():
        print("In static method:Parent")

class Child(Parent):
    pass

obj1 = Child()

obj1.DispData()
obj1.printData()
obj1.getData()
