class A:
    def __init__(self):
        print("In Class A")

class B(A):
    def __init__(self):
        print("In Class B")

class C(A):
    def __init__(self):
        print("In Class C")

class D(B,C):
    def __init__(self):
        print("In Class D")

class E(D):
    def __init__(self):
        print("In Class E")
print(E.mro())
