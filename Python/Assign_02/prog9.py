class College:
    def __init__(self):
        print("In college : Constructor")
    @staticmethod
    def Office():
        print("In College Office")

    @classmethod
    def DepartmentLibrary(cls):
        print("In Depart Library")

    def ClassRoom(self):
        print("In classRoom")

obj = College()
obj.Office()
obj.DepartmentLibrary()
obj.ClassRoom()
