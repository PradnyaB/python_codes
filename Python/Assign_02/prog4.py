class Parent:
    def __init__(self,x,y):
        self.x = x
        self.y = y

        print("In Parent Constructor")

    def DispData(self):
        print("In DispData : Parent")
        print(self.x)
        print(self.y)
class Child(Parent):

    def DispData(self):
        print("In DispData : Child")
        print(self.x)
        print(self.y)

x = int(input("Enter value of x: "))
y = int(input("Enter value of y: "))
obj = Child(x,y)
obj.DispData()







