class IPL:
    def __init__(self):
        print("In constructor : IPL")

    def Origin(self):
        print("India")

class MI(IPL):
    def __init__(self):
        print("In MumbaiIndians Constructor")

    def Captain(self):
        print("Captain : Rohit ")

class CSK(IPL):
    def __init__(self):
        print("In CSK Constructor")

    def Captain(self):
        print("Captain : MSD")

class RCB(IPL):
    def __init__(self):
        print("In RCB Constructor")

    def Captain(self):
        print("Captain : Virat ")

obj = IPL()
obj.Origin()

obj2 = MI()
obj2.Captain()

obj3 = CSK()
obj3.Captain()

obj4 = RCB()
obj4.Captain()

obj2.Origin()






