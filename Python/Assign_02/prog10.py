class Mahindra:
    def __init__(self):
        print("In Parent Constructor: Mahindra")
    def Models(self):
        print("1.Mahindra")
        print("2.Thar")

class Scorpio(Mahindra):
    def __init__(self):
        print("In Scorpio : Constructor")

    def getInfo(self):
        print("Diesel Engine 2198 cc")
        print("Model: Z8 Diesel")

class Thar(Mahindra):
    def __init__(self):
        print("In Thar: Constructor")

    def getInfo(self):
        print("Petrol Engine 1997cc ")
        print("Model : Lx4")

obj1 = Mahindra()
obj1.Models()

obj2 = Scorpio()
obj2.getInfo()

obj3 = Thar()
obj3.getInfo()
