class Parent1:
    '''    
    def __init__(self):
        print("In parent-1 constructor")
'''
    def dispData(self):
        print("In dispData of parent1")
        
class Parent2:
        
    def __init__(self):
        print("In parent-2 constructor")

    def dispData(self):
        print("In dispData of parent2")

    def printData(self):
        print("In pritData Parent 2")


class Child(Parent1, Parent2):
    pass

obj1 = Child()

obj1.dispData()

print(Child.mro())
