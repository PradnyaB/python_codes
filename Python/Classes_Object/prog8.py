class A:
    def fun(self):
        print("In fun:A")


class B:
    def fun(self):
        print("In fun:B")
class C:
    def fun(self):
        print("In fun:C")
class E(A,B):
    def fun(self):
        print("In fun:E")
class F(B,C):
    def fun(self):
        print("In fun:F")
class G(E,F):
    def fun(self):
        print("In fun:F")
print(G.mro())
