class Parent:
    def __init__(self):
        print("In parent constructor")
        self.x = 10
        self.y = 20

    def display(self):
        print(self.x)
        print(self.y)
    @staticmethod
    def fun():
        print("In static method")


class Child(Parent):

    def __init__(self):
        super().__init__()
        print("In child constructor")
    def display(self):
        print("In child disp")


obj1 = Child()

obj1.display();
obj1.fun()
